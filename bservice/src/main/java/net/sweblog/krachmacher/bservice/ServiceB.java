package net.sweblog.krachmacher.bservice;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import java.util.logging.Level;
import java.util.logging.Logger;

@Singleton
public class ServiceB
{
    private Logger logger = Logger.getLogger(ServiceB.class.getSimpleName());

    private int counter = 0;

    @Schedule(second = "*/5", minute = "*", hour = "*",
              persistent = false, timezone = "UTC")
    public void doLogging()
    {
        logger.log(Level.INFO, "counter=" + counter++);

    }
}
