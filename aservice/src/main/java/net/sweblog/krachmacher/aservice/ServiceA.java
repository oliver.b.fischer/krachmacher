package net.sweblog.krachmacher.aservice;

import org.joda.time.DateTime;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import java.util.logging.Level;
import java.util.logging.Logger;

@Singleton
public class ServiceA
{
    private Logger logger = Logger.getLogger(ServiceA.class.getSimpleName());

    private long counter = 0;

    @Schedule(second = "*/3", minute = "*", hour = "*",
              persistent = false, timezone = "UTC")
    public void doLogging()
    {
        for (int i = 10; i > 0; --i)
        {
            logger.log(Level.INFO, "counter="+ counter++);
        }
    }

}
